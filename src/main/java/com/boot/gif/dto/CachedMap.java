package com.boot.gif.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Component
public class CachedMap {
    Map<String, Map<String, List<String>>> cachedMap;

    public CachedMap() {
        cachedMap = new HashMap<>();
    }
}
