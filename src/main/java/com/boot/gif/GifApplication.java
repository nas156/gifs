package com.boot.gif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.PropertySource;


@PropertySource({"classpath:persistence-${envTarget:dev}.properties"})
@ConfigurationPropertiesScan
@SpringBootApplication
public class GifApplication {

    public static void main(String[] args) {
        SpringApplication.run(GifApplication.class, args);
    }

}
