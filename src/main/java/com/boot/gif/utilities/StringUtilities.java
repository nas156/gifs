package com.boot.gif.utilities;

import org.springframework.stereotype.Component;

@Component
public class StringUtilities {
    public String concatenateStrings(String... strings) {
        StringBuilder stringBuffer = new StringBuilder();
        for (String s : strings) {
            stringBuffer.append(s);
        }
        return stringBuffer.toString();
    }
}
