package com.boot.gif.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GiphyKeyConfiguration {
    @Bean
    public GiphyKey giphyKey(GiphyKeyCredentialsConfigurationProperties giphyKeyCredentialsConfigurationProperties) {
        return new GiphyKey(giphyKeyCredentialsConfigurationProperties.getKey());
    }
}
