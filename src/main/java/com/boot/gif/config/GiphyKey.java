package com.boot.gif.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public class GiphyKey {
    private final String KEY;

    public GiphyKey(String key) {
        this.KEY = key;

        log.info("Giphy Key is set");
    }
}
