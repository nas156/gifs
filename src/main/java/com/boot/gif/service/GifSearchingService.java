package com.boot.gif.service;

import com.boot.gif.dto.GifsDTO;
import com.boot.gif.utilities.StringUtilities;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class GifSearchingService {

    final MemoryCacheService memoryCacheService;
    final FilesCacheService filesCacheService;
    final HelperService helperService;
    final StringUtilities stringUtilities;

    public GifSearchingService(StringUtilities stringUtilities, HelperService helperService, FilesCacheService filesCacheService, MemoryCacheService memoryCacheService) {
        this.stringUtilities = stringUtilities;
        this.helperService = helperService;
        this.filesCacheService = filesCacheService;
        this.memoryCacheService = memoryCacheService;
    }


    public ResponseEntity<String> searchGif(String query, String username, boolean force) {
        if (!force && memoryCacheService.isQueryCachePresent(username, query)) {
            log.info("Found in memory cache");
            return getInCache(username, query);
        }
        File pathToFiles = new File(stringUtilities.concatenateStrings("bsa_giphy/users/", username, "/", query));

        if (pathToFiles.exists()) {
            memoryCacheService.addToCache(username, query, helperService.getRandomFile(helperService.getFilesFromStorage(pathToFiles)));
            return getInCache(username, query);
        }
        return ResponseEntity.status(404).body("No gif is found for your request");
    }

    private ResponseEntity<String> getInCache(String username, String query) {
        return ResponseEntity.status(HttpStatus.OK).body(helperService.getRandomFile(memoryCacheService.getCacheForQuery(username, query)));
    }

    public ResponseEntity<List<GifsDTO>> getAllUsersGifs(String username) {
        List<GifsDTO> allFiles = new ArrayList<>();
        helperService.getFilesFromStorage(new File("bsa_giphy/users/" + username)).stream().filter(this::isFileIsNotCSV).map(File::new).forEach(x -> allFiles.add(new GifsDTO(x.getName(), helperService.getFilesFromStorage(x))));
        return ResponseEntity.status(HttpStatus.OK).body(allFiles);
    }

    private boolean isFileIsNotCSV(String filename){
        return !filename.contains(".csv");
    }

    public ResponseEntity<Set<String>> getAllGifs() {
        Set<String> res = new HashSet<>();
        filesCacheService.getAllcachedFiles().forEach(x -> res.addAll(x.getPaths()));
        helperService.getFilesFromStorage(new File("bsa_giphy/users/")).stream().filter(this::isFileIsNotCSV)
                .flatMap(x -> helperService.getFilesFromStorage(new File(x)).stream())
                .forEach(x -> res.addAll(helperService.getFilesFromStorage(new File(x))));
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }


}
