package com.boot.gif.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class HelperService {

    public ResponseEntity<String> initFS() {
        try {
            this.createAllDirectories();
            return ResponseEntity.status(HttpStatus.OK).body("Body is empty");

        } catch (Exception e) {
            return ResponseEntity.status(500).body("Body is empty");
        }
    }

    private void createAllDirectories() {
        var root = "bsa_giphy";
        var cacheDir = root + "/cache";
        var usersDir = root + "/users";
        if (!(new File(root).exists())) {
            new File(cacheDir).mkdirs();
            new File(usersDir).mkdirs();
        } else {
            this.createDir(cacheDir);
            this.createDir(usersDir);
        }
    }

    private void createDir(String dirName) {
        var dir = new File(dirName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    public List<String> getFilesFromStorage(File path) {
        try {
            return Stream.of(Objects.requireNonNull(path.listFiles())).map(File::getAbsolutePath).collect(Collectors.toList());

        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public String getRandomFile(List<String> list) {
        if (list.size() == 1) {
            return list.get(0);
        }
        return list.get((int) (Math.random() * ((list.size() - 1) - 1) + 0));
    }

    public void copyFile(File source, File dest) throws IOException {
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destChannel = new FileOutputStream(dest).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert sourceChannel != null;
            sourceChannel.close();
            assert destChannel != null;
            destChannel.close();
        }
    }
}
