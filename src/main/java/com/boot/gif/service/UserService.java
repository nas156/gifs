package com.boot.gif.service;

import au.com.bytecode.opencsv.CSVWriter;
import com.boot.gif.dto.HistoryDTO;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserService {

    final MemoryCacheService memoryCacheService;
    private final String USER_FOLDER_TEMPLATE = "bsa_giphy/users/";

    public UserService(MemoryCacheService memoryCacheService) {
        this.memoryCacheService = memoryCacheService;
    }

    public void createUserFolder(String username) {
        new File(USER_FOLDER_TEMPLATE + username).mkdirs();

    }

    public boolean isExistingUserFolder(String username) {
        return new File(USER_FOLDER_TEMPLATE + username).exists();
    }

    public boolean isExistingFile(String pathToFile) {
        return new File(pathToFile).exists();
    }

    public void writeToCSV(String user, String query, String path) {
        String csv = USER_FOLDER_TEMPLATE + user + "/history.csv";
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(csv, true));
            String[] record = {new Date().toString(), query, path};
            writer.writeNext(record);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ResponseEntity<String> cleanCSV(String username) {
        String csv = USER_FOLDER_TEMPLATE + username + "/history.csv";
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(csv));
            writer.close();
            return ResponseEntity.status(HttpStatus.OK).body("Cleaned");
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Failed to clean");
        }
    }

    public ResponseEntity<List<HistoryDTO>> getUserHistory(String username) {
        String csvFile = "bsa_giphy/users/" + username + "/history.csv";
        BufferedReader br = null;
        String line = "";
        List<HistoryDTO> result = new ArrayList<>();


        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] splited = line.split(",");
                result.add(new HistoryDTO(splited[0], splited[1], splited[2]));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    public ResponseEntity<String> deleteUserFiles(String username) {
        try {
            FileUtils.deleteDirectory(new File(USER_FOLDER_TEMPLATE + username));
            StringBuilder message = new StringBuilder("Deleted");
            if (!memoryCacheService.deleteUsersData(username)) {
                message.append(", ram cache was not present");
            }
            return ResponseEntity.status(HttpStatus.OK).body(message.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Failed to delete");
        }
    }

    public ResponseEntity<String> deleteUsersCacheData(String username, String query) {
        if (query != null && !query.isBlank()) {
            if (!memoryCacheService.deleteUsersQueryData(username, query)) {
                return ResponseEntity.status(400).body("Failed to delete");
            }
            return ResponseEntity.status(HttpStatus.OK).body("Users query cache deleted");
        } else {
            memoryCacheService.deleteUsersData(username);
            return ResponseEntity.status(HttpStatus.OK).body("All users cache deleted");
        }
    }
}
