package com.boot.gif.service;

import com.boot.gif.dto.CachedMap;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemoryCacheService {

    final CachedMap cachedMap;
    private final Map<String, Map<String, List<String>>> cacheMap;

    public MemoryCacheService(CachedMap cachedMap) {
        this.cachedMap = cachedMap;
        this.cacheMap = cachedMap.getCachedMap();
    }

    public void addToCache(String user, String query, String path) {
        if (cacheMap.containsKey(user)) {
            Map<String, List<String>> queryMap = cacheMap.get(user);
            if (queryMap.containsKey(query) && !queryMap.get(query).contains(path)) {
                queryMap.get(query).add(path);
            } else {
                queryMap.put(query, createNewList(path, query));
            }
        } else {
            Map<String, List<String>> queryMap = new HashMap<>();
            queryMap.put(query, createNewList(path, query));
            cacheMap.put(user, queryMap);
        }
    }

    public Map<String, Map<String, List<String>>> getAllCache() {
        return cacheMap;
    }

    public boolean deleteUsersData(String username) {
        if (isUserCachePresent(username)) {
            cacheMap.remove(username);
            return true;
        }
        return false;
    }

    public boolean deleteUsersQueryData(String username, String query) {
        if (isQueryCachePresent(username, query)) {
            cacheMap.get(username).remove(query);
            return true;
        }
        return false;
    }

    public Map<String, List<String>> getCacheForUser(String username) {
        return cacheMap.get(username);
    }

    public boolean isUserCachePresent(String username) {
        return this.cacheMap.containsKey(username);
    }

    public List<String> getCacheForQuery(String username, String query) {
        return getCacheForUser(username).get(query);
    }

    public boolean isQueryCachePresent(String username, String query) {
        if (isUserCachePresent(username)) {
            return getCacheForUser(username).containsKey(query);
        }
        return false;
    }

    private List<String> createNewList(String path, String query) {
        List<String> setForQuery = new ArrayList<>();
        setForQuery.add(path);
        return setForQuery;
    }

    private boolean isPresentUser(String user) {
        return cacheMap.containsKey(user);
    }
}
