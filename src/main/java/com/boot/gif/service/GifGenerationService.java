package com.boot.gif.service;


import com.boot.gif.config.GiphyKey;
import com.boot.gif.dto.GifsDTO;
import com.boot.gif.utilities.StringUtilities;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;


@Service
public class GifGenerationService {

    final MemoryCacheService memoryCacheService;
    final StringUtilities stringUtilities;
    final FilesCacheService filesCacheService;
    final UserService userService;
    final GiphyKey giphyKey;
    final HelperService helperService;

    private final String API_URL_TEMPLATE;

    public GifGenerationService(GiphyKey giphyKey, UserService userService, FilesCacheService filesCacheService, StringUtilities stringUtilities, HelperService helperService, MemoryCacheService memoryCacheService) {
        this.giphyKey = giphyKey;
        this.userService = userService;
        this.filesCacheService = filesCacheService;
        this.stringUtilities = stringUtilities;
        this.API_URL_TEMPLATE = stringUtilities.concatenateStrings("http://api.giphy.com/v1/gifs/random?api_key=", giphyKey.getKEY(), "&tag=");
        this.helperService = helperService;
        this.memoryCacheService = memoryCacheService;
    }

    public ResponseEntity<GifsDTO> generateGifToCache(String query, String header) {
        String apiUrl = stringUtilities.concatenateStrings(API_URL_TEMPLATE, query);
        String gifId;
        try {
            gifId = this.getGifId(apiUrl);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GifsDTO());
        }
        String pathToSave = stringUtilities.concatenateStrings("bsa_giphy/cache/", query, "/", query, "_", gifId, ".gif");
        File cacheFile = new File(pathToSave);
        String pathToDownload = stringUtilities.concatenateStrings("http://i.giphy.com/media/", gifId, "/giphy.gif");
        if (!filesCacheService.isExistingCacheFolder(query)) {
            filesCacheService.createCacheFolder(query);
        }
        try {
            cacheFile.createNewFile();
            saveImage(pathToDownload, pathToSave);
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new GifsDTO());
        }
        File[] files = new File("bsa_giphy/cache/" + query).listFiles();
        List<String> paths = new ArrayList<>();
        Stream.of(Objects.requireNonNull(files)).map(File::getAbsolutePath).forEach(paths::add);
        return ResponseEntity.status(HttpStatus.OK).body(new GifsDTO(query, paths));

    }

    private String findGifInCache(String username, String query){
        String path = helperService.getRandomFile(helperService.getFilesFromStorage(new File(stringUtilities.concatenateStrings("bsa_giphy/cache/", query))));
        File savedGif = new File(path);
        String pathToSave = stringUtilities.concatenateStrings("bsa_giphy/users/", username, "/", query, "/", savedGif.getName());
        File fileToSave = new File(pathToSave);
        filesCacheService.createNewFile(fileToSave, savedGif);
        String userGifFile = fileToSave.getAbsolutePath();
        memoryCacheService.addToCache(username, query, userGifFile);
        userService.writeToCSV(username, query, userGifFile);
        return userGifFile;
    }

    public ResponseEntity<String> generateGif(String username, String query, String header, boolean force) {
        if (!force && filesCacheService.isExistingCacheFolder(query)) {
            return ResponseEntity.status(HttpStatus.OK).body(findGifInCache(username, query));
        }
        String apiUrl = API_URL_TEMPLATE + query;
        String gifId;
        try {
            gifId = this.getGifId(apiUrl);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unreal to generate gif for your request");
        }
        String fileToSave = stringUtilities.concatenateStrings("bsa_giphy/cache/", query, "/", query, "_", gifId, ".gif");
        File cacheFile = new File(fileToSave);
        String pathToDownload = stringUtilities.concatenateStrings("http://i.giphy.com/media/", gifId, "/giphy.gif"); //"http://i.giphy.com/media/" + gifId + "/giphy.gif";
        try {
            cacheFile.getParentFile().mkdirs();
            cacheFile.createNewFile();
            saveImage(pathToDownload, fileToSave);
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Failed to save gif");
        }
        String absPath = findGifInCache(username, query);
        return ResponseEntity.status(HttpStatus.OK).body(absPath);
    }

    private String getGifId(String apiUrl) throws IOException {
        URL url = new URL(apiUrl);
        URLConnection request = url.openConnection();
        request.connect();
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject rootobj = root.getAsJsonObject();
        return rootobj.getAsJsonObject("data").get("id").getAsString();
    }


    private void saveImage(String imageUrl, String destinationFile) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        File file = new File(destinationFile);
        OutputStream os = new FileOutputStream(destinationFile);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }


}
