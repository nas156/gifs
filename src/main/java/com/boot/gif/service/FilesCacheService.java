package com.boot.gif.service;

import com.boot.gif.dto.GifsDTO;
import com.boot.gif.utilities.StringUtilities;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FilesCacheService {

    final HelperService helperService;
    final StringUtilities stringUtilities;
    private final String CACHE_FOLDER_TEMPLATE = "bsa_giphy/cache/";

    public FilesCacheService(HelperService helperService, StringUtilities stringUtilities) {
        this.helperService = helperService;
        this.stringUtilities = stringUtilities;
    }

    public void createCacheFolder(String query) {
        new File(CACHE_FOLDER_TEMPLATE + query).mkdirs();

    }

    public void createNewFile(File file1, File file2) {
        if (!file1.exists()){
            try {
                file1.getParentFile().mkdirs();
                file1.createNewFile();
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        try {
            file1.createNewFile();
            helperService.copyFile(file2, file1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ResponseEntity<String> deleteCache() {
        try {
            FileUtils.cleanDirectory(new File(CACHE_FOLDER_TEMPLATE));
            return ResponseEntity.status(HttpStatus.OK).body("Deleted");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Unable to delete");
        }
    }

    public boolean isExistingCacheFolder(String query) {
        return new File(CACHE_FOLDER_TEMPLATE + query).exists();
    }

    public ResponseEntity<List<GifsDTO>> getAllCacheFiles(String query) {
        List<GifsDTO> allFiles = new ArrayList<>();
        if (query == null || query.isEmpty()) {
            allFiles = this.getAllcachedFiles();
        } else {
            allFiles.add(new GifsDTO(query, helperService.getFilesFromStorage(new File(stringUtilities.concatenateStrings("bsa_giphy/cache/", query)))));
        }
        return ResponseEntity.status(HttpStatus.OK).body(allFiles);
    }

    public List<GifsDTO> getAllcachedFiles(){
        List<GifsDTO> allFiles = new ArrayList<>();
        helperService.getFilesFromStorage(new File("bsa_giphy/cache")).stream().map(File::new).forEach(x -> allFiles.add(new GifsDTO(x.getName(), helperService.getFilesFromStorage(x))));
        return allFiles;
    }
}
