package com.boot.gif.controller;

import com.boot.gif.service.HelperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelperAPI {

    final HelperService helperService;

    @Autowired
    public HelperAPI(HelperService helperService) {
        this.helperService = helperService;
    }

    @PostMapping(value = "/init")
    public ResponseEntity<String> makeInitialFS(@RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY) {
        log.info("Post /init");
        return helperService.initFS();
    }

}
