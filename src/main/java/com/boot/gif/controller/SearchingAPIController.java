package com.boot.gif.controller;

import com.boot.gif.service.GifSearchingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@Slf4j
@RestController
public class SearchingAPIController {

    final GifSearchingService gifSearchingService;

    public SearchingAPIController(GifSearchingService gifSearchingService) {
        this.gifSearchingService = gifSearchingService;
    }


    @GetMapping("/gifs")
    public ResponseEntity<Set<String>> getAllFiles(@RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY) {
        log.info("Get /gifs");
        return gifSearchingService.getAllGifs();
    }


}
