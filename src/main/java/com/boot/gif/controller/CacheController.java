package com.boot.gif.controller;

import com.boot.gif.dto.GifsDTO;
import com.boot.gif.service.FilesCacheService;
import com.boot.gif.service.GifGenerationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class CacheController {

    private final GifGenerationService gifGenerationService;
    final FilesCacheService filesCacheService;

    public CacheController(FilesCacheService filesCacheService, GifGenerationService gifGenerationService) {
        this.filesCacheService = filesCacheService;
        this.gifGenerationService = gifGenerationService;
    }

    @GetMapping("/cache")
    public ResponseEntity<List<GifsDTO>> getCache(@RequestParam(required = false) String query,
                                                  @RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY) {
        log.info("Get /cache query:" + query);
        return filesCacheService.getAllCacheFiles(query);
    }

    @PostMapping("/cache/generate")
    public ResponseEntity<GifsDTO> generateGifToCache(@RequestHeader(name = "X-BSA-GIPHY", required = false) String X_BSA_GIPHY, @RequestParam String query) {
        log.info("Post /cache/generate query:" + query);
        return gifGenerationService.generateGifToCache(query, X_BSA_GIPHY);
    }

    @DeleteMapping("/cache")
    public ResponseEntity<String> deleteCache(@RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY) {
        log.info("Delete /cache");
        return filesCacheService.deleteCache();
    }
}
