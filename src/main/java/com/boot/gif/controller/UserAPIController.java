package com.boot.gif.controller;

import com.boot.gif.dto.GifsDTO;
import com.boot.gif.dto.HistoryDTO;
import com.boot.gif.service.GifGenerationService;
import com.boot.gif.service.GifSearchingService;
import com.boot.gif.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@Controller
@Validated
public class UserAPIController {

    final GifSearchingService gifSearchingService;
    final UserService userService;
    final GifGenerationService gifGenerationService;


    public UserAPIController(UserService userService, GifGenerationService gifGenerationService, GifSearchingService gifSearchingService) {
        this.userService = userService;
        this.gifGenerationService = gifGenerationService;
        this.gifSearchingService = gifSearchingService;
    }

    @GetMapping("/user/{id}/all")
    public ResponseEntity<List<GifsDTO>> getAllUsersGifs(@PathVariable @NotNull @NotBlank String id,
                                                         @RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY) {
        log.info("Get /user/{id}/all id:" + id);
        return gifSearchingService.getAllUsersGifs(id);
    }

    @GetMapping(value = "/user/{id}/history")
    public ResponseEntity<List<HistoryDTO>> getUserHistory(@PathVariable @NotNull @NotBlank String id,
                                           @RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY) {
        log.info("Get /user/{id}/history id:" + id);
        return userService.getUserHistory(id);
    }

    @GetMapping(value = "/user/{id}/search")
    public ResponseEntity<String> getGif(@RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY,
                                         @RequestParam String query, @PathVariable @NotNull @NotBlank String id,
                                         @RequestParam(required = false) boolean force) {
        log.info("Get user/{id}/search/ id:" + id + " query:" + query + " force:" + force);
        return gifSearchingService.searchGif(query, id, force);
    }

    @DeleteMapping("/user/{id}/reset")
    public ResponseEntity<String> deleteUsersCache(@PathVariable @NotNull @NotBlank String id,
                                                   @RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY,
                                                   @RequestParam(required = false) String query) {
        log.info("Delete /user/{id}/reset id:" + id + " query:" + query);
        return userService.deleteUsersCacheData(id, query);
    }

    @DeleteMapping("/user/{id}/clean")
    public ResponseEntity<String> deleteUsersGifs(@PathVariable @NotNull @NotBlank String id,
                                                  @RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY) {
        log.info("Delete /user/{id}/clean id:" + id);
        return userService.deleteUserFiles(id);
    }

    @DeleteMapping("/user/{id}/history/clean")
    public ResponseEntity<String> deleteUserHistory(@PathVariable @NotNull @NotBlank String id,
                                                    @RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY) {
        log.info("Delete /user/{id}/history/clean id:" + id);
        return userService.cleanCSV(id);
    }


    @PostMapping("user/{id}/generate")
    public ResponseEntity<String> generateGif(@RequestHeader(name = "X-BSA-GIPHY") String X_BSA_GIPHY,
                                              @RequestParam String query, @PathVariable @NotNull @NotBlank String id,
                                              @RequestParam(required = false) boolean force) {
        log.info("Post /user/{id}/generate id:" + id + " query:" + query + " force:" + force);
        return gifGenerationService.generateGif(id, query, X_BSA_GIPHY, force);
    }

}
